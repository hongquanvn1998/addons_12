# # -*- coding: utf-8 -*-

# # from odoo import models, fields, api
# import redis

redis_host = "localhost"
redis_port = 6379
redis_password = ""

# def hello_redis():
#     """ Example hello redis program """
#     try:
#         r = redis.StrictRedis(host = redis_host,port = redis_port,password = redis_password)
#         # r=redis.Redis
#         r.hset(1,"Hello Redis!!!")
#         # r.
#         print('qua day roi')
#         msg = r.get("msgdj")
#         print(msg)
#     except Exception as e:
#         print(e)

# if __name__ == '__main__':
#     hello_redis()

import logging
import redis
import random

logging.basicConfig()

r = redis.StrictRedis(host = redis_host,port = redis_port,password = redis_password)

hats = {f"{random.getrandbits(32)}": i for i in (
    {
        "color": "black",
        "price": 49.99,
        "style": "fitted",
        "quantity": 1000,
        "npurchased": 0,
    },
    {
        "color": "maroon",
        "price": 59.99,
        "style": "hipster",
        "quantity": 500,
        "npurchased": 0,
    },
    {
        "color": "green",
        "price": 99.99,
        "style": "baseball",
        "quantity": 200,
        "npurchased": 0,
    })
}
# with r.pipeline() as pipe:
#     for h_id, hat in hats.items():
#         pipe.hmset(h_id, hat)
#         print("ra")
#     pipe.execute()

class OutOfStockError(Exception):
    """Raised when PyHats.com is all out of today's hottest hat"""

def buyitem(r: redis.Redis, itemid: int) -> None:
    with r.pipeline() as pipe:
        error_count = 0
        while True:
            try:
                # Get available inventory, watching for changes
                # related to this itemid before the transaction
                pipe.watch(itemid)
                nleft: bytes = r.hget(itemid, "quantity")
                if nleft > b"0":
                    pipe.multi()
                    pipe.hincrby(itemid, "quantity", -1)
                    pipe.hincrby(itemid, "npurchased", 1)
                    pipe.execute()
                    break
                else:
                    # Stop watching the itemid and raise to break out
                    pipe.unwatch()
                    raise OutOfStockError(
                        f"Sorry, {itemid} is out of stock!"
                    )
            except redis.WatchError:
                # Log total num. of errors by this user to buy this item,
                # then try the same process again of WATCH/HGET/MULTI/EXEC
                error_count += 1
                logging.warning(
                    "WatchError #%d: %s; retrying",
                    error_count, itemid
                )
    return None