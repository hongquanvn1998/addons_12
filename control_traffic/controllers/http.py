import json
import werkzeug.exceptions
import logging

import odoo
from odoo import http
from odoo.http import WebRequest,HttpRequest,JsonRequest,Response,routing_map

import redis
import requests
from odoo.http import request
from odoo.addons.jwt_provider_odoo.jwt_http import jwt_http
from odoo.addons.jwt_provider_odoo.validator import validator

_logger = logging.getLogger(__name__)

class Rooting(http.Root):
    def get_request(self, httprequest):
        r = redis.StrictRedis(host = "localhost",port = 6379,password = "",db=0)
        openurl = 'http://ipinfo.io/json'
        new_route = httprequest.base_url
        accept_request = True

        with r.pipeline() as pipe:
            try:
                if r.exists(new_route):
                    pipe.watch(new_route)
                    seconds_10 = 0 if pipe.get("10:%s"%new_route) == None else int(pipe.get("10:%s"%new_route).decode("utf8"))
                    if pipe.exists("10:%s"%new_route) and seconds_10 <= odoo.tools.config.options['limit_traffic']:
                        pipe.multi()
                        pipe.incrby("10:%s"%new_route)
                        data = requests.get(openurl)
                        data = json.loads(data.content)
                        pipe.hsetnx(new_route,data["ip"],data)
                    elif pipe.exists("10:%s"%new_route) and seconds_10 > odoo.tools.config.options['limit_traffic']:
                        msg = 'Forbidden Error: %s' % (new_route)
                        _logger.info('%s: %s', httprequest.path, msg)
                        accept_request = False
                        return werkzeug.exceptions.BadRequest(msg)
                    elif pipe.exists("10:%s"%new_route) == False:
                        pipe.setex("10:%s"%new_route,10,1)
                    pipe.hincrby(new_route,"count",1)
                    
                else:
                    data = requests.get(openurl)
                    data = json.loads(data.content)
                    info = {}
                    info["url"] = new_route
                    info["count"] = 1
                    info[data["ip"]] = data
                    pipe.hmset(new_route,info)
                    pipe.setex("10:%s"%new_route,10,1)
                pipe.execute()
            except Exception as e:
                pipe.execute()
                logging.warning( "WatchError %s; retrying", new_route )
        # deduce type of request
        if accept_request:
            if httprequest.args.get('jsonp'):
                return JsonRequest(httprequest)
            if httprequest.mimetype in ("application/json", "application/json-rpc"):
                return JsonRequest(httprequest)
            else:
                return HttpRequest(httprequest)

class ReWebRequest(http.WebRequest):
    def set_handler(self, endpoint, arguments, auth):
        arguments ={k: v for k, v in arguments.items()
                         if not k.startswith("_ignored_")}
        self.endpoint_arguments = arguments
        self.endpoint = endpoint
        self.auth_method = auth
        ip_address = request.httprequest.environ['REMOTE_ADDR']
        http_method, body, headers, token = jwt_http.parse_request()
        
        if 'jwt' in self.endpoint.routing:
            if self.endpoint.routing['jwt'] and 'Authorization' in headers:
                result = validator.verify_token(token,ip_address)
                if not result['status']:
                    raise werkzeug.exceptions.BadRequest('Cannot verify token')
            elif self.endpoint.routing['jwt'] and 'Authorization' not in headers:
                raise werkzeug.exceptions.BadRequest('Token cannot null')


http.WebRequest.set_handler = ReWebRequest.set_handler

http.Root.get_request = Rooting.get_request