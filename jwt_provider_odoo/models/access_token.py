from ipaddress import ip_address
from odoo import models, fields, api
from datetime import datetime, timedelta
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

class JwtAccessToken(models.Model):
    _name = 'jwt_provider.access_token'
    _description = 'Store user access token for one-time-login'

    token = fields.Char('Access Token', required=True)
    user_id = fields.Many2one('res.users', string='User', required=True, ondelete='cascade')
    expires = fields.Datetime('Expires', required=True)

    is_expired = fields.Boolean(compute='_compute_is_expired')

    count = fields.Integer(string='Count', default=0)
    limit = fields.Integer(string='Limit After', default=0)
    agent = fields.Char(string='Agent')
    ip_address = fields.Char(string='Agent')


    @api.depends('expires')
    def _compute_is_expired(self):
        for token in self:
            if token.expires:
                token.is_expired = datetime.now() > token.expires
    
    @api.model
    def _add_count(self,id):
        jwt = self.env['jwt_provider.access_token'].sudo().search([
            ('id', '=', id)
        ])
        jwt.sudo().write({'count':jwt.count + 1})
    
    @api.model
    def _is_expired(self,id):
        jwt = self.env['jwt_provider.access_token'].sudo().search([
            ('id', '=', id)
        ])
        jwt.sudo().write({'is_expired':True})
