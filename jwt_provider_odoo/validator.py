import logging
import jwt
import re
import datetime
import traceback
import os
import odoo
from odoo import http, service, registry, SUPERUSER_ID
from odoo.http import request
from odoo.tools import DEFAULT_SERVER_DATETIME_FORMAT

_logger = logging.getLogger(__name__)

regex = r"^[a-z0-9!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$"


class Validator:
    def is_valid_email(self, email):
        return re.search(regex, email)

    def key(self):
        # return os.environ.get('ODOO_JWT_KEY')
        return odoo.tools.config.options['odoo_jwt_key']

    def create_token(self, user, password,ip_address):
        try:
            exp = datetime.datetime.utcnow() + datetime.timedelta(days=30)
            payload = {
                'exp': exp,
                'iat': datetime.datetime.utcnow(),
                'sub': user['id'],
                'lgn': user['login'] or user['email'],
                'password': password,
                'ip_address':ip_address
            }
            token = jwt.encode(
                payload,
                str(self.key()),
                algorithm='HS256'
            )

            self.save_token(token, user['id'], exp,ip_address)

            # return token.decode('utf-8')
            return token
        except Exception as ex:
            _logger.error(ex)
            raise
    
    def create_refresh_token(self, user, password,ip_address):
        try:
            exp = datetime.datetime.utcnow() + datetime.timedelta(days=30)
            payload = {
                'lgn': user['login'] or user['email'],
                'password': password,
                'ip_address':ip_address
            }
            token = jwt.encode(
                payload,
                str(self.key()),
                algorithm='HS256'
            )
            return token
        except Exception as ex:
            _logger.error(ex)
            raise

    def save_token(self, token, uid, exp,ip_address):
        request.env['jwt_provider.access_token'].sudo().create({
            'user_id': uid,
            'expires': exp.strftime(DEFAULT_SERVER_DATETIME_FORMAT),
            'token': token,
            'ip_address':ip_address
        })

    def verify(self, token,ip_address=None):
        record = request.env['jwt_provider.access_token'].sudo().search([
            ('token', '=', token)
        ])

        if len(record) != 1:
            _logger.info('not found %s' % token)
            return False

        if ip_address is not None and ip_address != record.ip_address:
            _logger.info('Ip address not right')
            return False

        if record.is_expired:
            _logger.info('Token is expired')
            return False

        if record.limit > 0 and record.count > record.limit:
            request.env['jwt_provider.access_token'].sudo()._is_expired(record.id)
            _logger.info('Token is limited')
            return False

        if datetime.datetime.now() > record.expires:
            request.env['jwt_provider.access_token'].sudo()._is_expired(record.id)
            _logger.info('Token is expired')
            return False

        request.env['jwt_provider.access_token'].sudo()._add_count(record.id)
        return record.user_id

    def verify_token(self, token, ip_address=None):
    
        try:
            result = {
                'status': False,
                'message': None,
            }
            key_pass = self.key()
            payload = jwt.decode(token, str(self.key()), 'HS256')
            if not self.verify(token,ip_address):
                return self.errorToken()

            uid = request.session.authenticate(request.session.db, payload['lgn'], payload['password'])
            if not uid:
                return self.errorToken()

            result['status'] = True
            return result
        except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, Exception) as e:
            return self.errorToken()

    def decode_jwt(self,token):
        try:
            result = {
                'status': False,
                'message': None,
            }
            key_pass = self.key()
            payload = jwt.decode(token, str(self.key()), 'HS256')
            if not self.verify(token):
                return self.errorToken()
            result['status'] = True
            result['email'] = payload['lgn']
            result['password'] = payload['password']

            return result
        except (jwt.ExpiredSignatureError, jwt.InvalidTokenError, Exception) as e:
            return self.errorToken()

    def errorToken(self):
        return {
            'message': 'Token invalid or expired',
            'code': 498,
            'status': False
        }

validator = Validator()