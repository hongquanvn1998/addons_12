# -*- coding: utf-8 -*-
{
    'name': "Jwt Provider",

    'summary': """
        Provide a simple rest using jwt for odoo 14""",

    'description': """
        Key features:
         - jwt supported
         - Login via endpoint
    """,

    'author': "saturnlai",
    'website': "http://github.com/daolvcntt",

    # Categories can be used to filter modules in modules listing
    # Check https://github.com/odoo/odoo/blob/master/odoo/addons/base/module/module_data.xml
    # for the full list
    'category': 'Uncategorized',
    'version': '0.1',

    # any module necessary for this one to work correctly
    'depends': ['web', 'auth_signup'],

    'external_dependencies': {
        'python': ['jwt', 'simplejson'],
    },
    'data': [
        'security/jwt_provider_odoo_security.xml',
        'security/ir.model.access.csv',
        'views/user_view.xml',
    ],
    'css': [
        'static/src/css/jwt.css',
    ],
    'demo': [
    ],
}